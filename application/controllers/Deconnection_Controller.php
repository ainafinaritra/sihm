<?php

class Deconnection_Controller extends CI_Controller {
	
	public function deconnecter(){
		$this->load->helper('url');
		$this->load->library('cart');
		$this->cart->update(null);
		session_destroy();
		redirect('', 'refresh');
	}
}

?>
