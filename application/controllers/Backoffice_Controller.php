<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backoffice_Controller extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
	}
	public function index()
	{
		$this->load->view('backOfficeLogin_View');	
	}
	public function login(){
		//$this->load->helper('url');
		$this->load->model('Employe_Model');
		
		$login = $this->input->post("login");
		$password = $this->input->post("password");
		
		$where = "where LOGIN like '%s' and MOTDEPASSE = SHA1('%s') and IDPROFIL = 2";
		$where = sprintf($where, $login, $password);
		$result = $this->Employe_Model->select2($where);
		
		if(count($result) == 0){
			$this->load->view('backOfficeLogin_View');
		}else{
			
			$this->load->model('FunctionDashBoard_Model');
			$data['Milay'] = $this->FunctionDashBoard_Model->getTop5PlatsCommander();
			$data['TsyMilay'] = $this->FunctionDashBoard_Model->getTop5PlatsNonCommander();
			$data['FrequenceMensuel'] = $this->FunctionDashBoard_Model->getFrequenceMensuel();
			$data['FrequenceJournaliaire'] = $this->FunctionDashBoard_Model->getFrequenceJournalier();
			
			// var_dump($data);
			$this->load->view('backOffice_View', $data);
		}
		
	}
	public function dash(){
		$this->load->helper('url');	
		$this->load->model('FunctionDashBoard_Model');
			$data['Milay'] = $this->FunctionDashBoard_Model->getTop5PlatsCommander();
			$data['TsyMilay'] = $this->FunctionDashBoard_Model->getTop5PlatsNonCommander();
			$data['FrequenceMensuel'] = $this->FunctionDashBoard_Model->getFrequenceMensuel();
			$data['FrequenceJournaliaire'] = $this->FunctionDashBoard_Model->getFrequenceJournalier();
			
			$this->load->view('backOffice_View', $data);
	}
	public function parametre(){
		$this->load->helper('url');	
		$this->load->view('backOfficeParametre_View');
	}
		
}
