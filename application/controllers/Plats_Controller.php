<?php
    class Plats_Controller extends CI_Controller{
        function __construct() { 
			parent::__construct(); 
            $this->load->database(); 
            $this->load->helper('url'); 
        }
        
        public function getToday(){
            $query = $this->db->query(" select dayofweek(now()) as dayOfWeek ");
		    $resultat = $query->result_array();
		return $resultat[0]['dayOfWeek'];
        }
        public function getDayMenu(){
            $day = $this->getToday();
            $this->load->model('Plats_Model');
            $result = $this->Plats_Model->select2(" where jour like '%".$day."%'");
            $data['vue'] = 'PlatJour.php';
            $data['plats'] = $result;
            $this->load->view('Template', $data);
        }

        public function chercher(){
            $toFind = $this->input->get('search');
            $this->load->model('Plats_Model');
            $result = $this->Plats_Model->select2(" where NOMPLAT like '%".$toFind."%'");
            $data['vue'] = 'Result_View.php';
            $data['plats'] = $result;
            $this->load->view('Template', $data);
        }
    }
?>