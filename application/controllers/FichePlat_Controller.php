<?php

class FichePlat_Controller extends CI_Controller {
	
	public function index()
	{
        $this->load->helper('url');
        $this->load->model('Plats_Model');
        $this->load->model('Tableresto_Model');

        $idplat = $this->input->get('idplat');
        $data['fiche'] = $this->Plats_Model->select($idplat);
        $data['tables'] = $this->Tableresto_Model->select2("");
        $data['vue'] = 'FichePlat_View.php';
		$this->load->view('Template', $data);	
	}
}
?>