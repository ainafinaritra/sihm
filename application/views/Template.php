<!DOCTYPE html>
<html lang="en">
<head>
<title>Rajaonah Hary Ny Aina P11A ETU756 - Raveloson Finaritra Mickaelle P11B ETU846</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="The Venue template project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/bootstrap-4.1.2/bootstrap.min.css">
<link href="<?php echo base_url(); ?>assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/OwlCarousel2-2.2.1/animate.css">
<link href="<?php echo base_url(); ?>assets/plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/plugins/jquery-datepicker/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/plugins/jquery-timepicker/jquery.timepicker.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/responsive.css">

</head>

<?php
$view = 'Plats_View.php';
$entrees = array();
$resistances = array();
$desserts = array();

if(isset($platsEntree)){
    $entrees = $platsEntree;
}
if(isset($platsResistance)){
    $resistances = $platsResistance;
}
if(isset($platsDesserts)){
    $desserts = $platsDesserts;
}
if(isset($vue)){
    $view = $vue;
}
?>

<body>

<div class="super_container">
	
	<!-- Header -->

	<header class="header">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_content d-flex flex-row align-items-center justify-content-start">
						<div class="logo">
							<a href="#">
								<div>The Venue</div>
								<div>restaurant</div>
							</a>
						</div>
						<nav class="main_nav">
							<ul class="d-flex flex-row justify-content-start">
								<li><a href="home">Home</a></li>
								<li><a href="platJour">Plat du jour</a></li>
                                <li><a href="backOffice">Back Office</a></li>
								<li><a href="commandes">Commandes reçues</a></li>
								<li><a href="impayes">Impayées</a></li>
								<li><a href="deconnection">Deconnexion</a></li>
							</ul>
							<form action = "recherche" method="get">
								<input type="text" name="search" placeholder="Search">
								<button class ="btn btn-primary">Rechercher</button>
							</form>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- Hamburger -->
	
	<div class="hamburger_bar trans_400 d-flex flex-row align-items-center justify-content-start">
		<div class="hamburger">
			<div class="menu_toggle d-flex flex-row align-items-center justify-content-start">
				<span>menu</span>
				<div class="hamburger_container">
					<div class="menu_hamburger">
						<div class="line_1 hamburger_lines" style="transform: matrix(1, 0, 0, 1, 0, 0);"></div>
						<div class="line_2 hamburger_lines" style="visibility: inherit; opacity: 1;"></div>
						<div class="line_3 hamburger_lines" style="transform: matrix(1, 0, 0, 1, 0, 0);"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Menu -->

	<div class="menu trans_800">
		<div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
			<ul>
				<li><a href="home">Home</a></li>
				<li><a href="platJour">Plat du jour</a></li>
				<li><a href="backOffice">Back Office</a></li>
				<li><a href="commandes">Commandes reçues</a></li>
				<li><a href="deconnection">Deconnexion</a></li>
			</ul>
		</div>
	</div>

	<!-- The Menu -->

	<div class="themenu">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="themenu_title_bar_container">
						<div class="themenu_stars text-center page_subtitle">Aina & Finaritra</div>
						<div class="themenu_rating text-center">
							<div class="rating_r rating_r_5"><i></i><i></i><i></i><i></i><i></i></div>
						</div>
						<div class="themenu_title_bar d-flex flex-column align-items-center justify-content-center">
							
						</div>
					</div>
				</div>
			</div>
			<div class="row themenu_row">
                <?php include($view); ?>
			</div>
		</div>		
	</div>


	<!-- Footer -->
	<footer class="footer">
		<div class="container">
			<div class="row">

				<!-- Footer Logo -->
				<div class="col-lg-3 footer_col">
					<div class="footer_logo">
						<div class="footer_logo_title">The Venue</div>
						<div class="footer_logo_subtitle">restaurant</div>
					</div>
					<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
<p style="line-height: 1.2;">Copyright &copy;Juillet 2019 All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
				</div>

				<!-- Footer About -->
				<div class="col-lg-6 footer_col">
					<div class="footer_about">
						<p>Rajaonah Hary Ny Aina P11A ETU756 - Raveloson Finaritra Mickaelle P11B ETU846</p>
					</div>
				</div>

				<!-- Footer Contact -->
				<div class="col-lg-3 footer_col">
					<div class="footer_contact">
						<ul>
							<li class="d-flex flex-row align-items-start justify-content-start">
								<div class="footer_contact_text">harynyainarajaonah01@gmail.com</div>
							</li>
							<li class="d-flex flex-row align-items-start justify-content-start">
								<div class="footer_contact_text">finaritra.mickaelle@gmail.com</div>
							</li>
							<li class="d-flex flex-row align-items-start justify-content-start">
								<div class="footer_contact_text">https://gitlab.com/ainafinaritra/sihm.git</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/styles/bootstrap-4.1.2/popper.js"></script>
<script src="<?php echo base_url(); ?>assets/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/greensock/TweenMax.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/greensock/TimelineMax.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/greensock/animation.gsap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/easing/easing.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/parallax-js-master/parallax.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-datepicker/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-timepicker/jquery.timepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
</body>
</html>