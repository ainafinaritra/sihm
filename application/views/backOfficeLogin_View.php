<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to BackOffice</title>
	<link rel="stylesheet" href = "<?php echo base_url();?>bootstrap/css/bootstrap.min.css">
    <link href="<?php echo base_url();?>bootstrap/css/mdb.min.css" rel="stylesheet">
	<link rel="stylesheet" href = "<?php echo base_url();?>bootstrap/css/design.css">
</head>
<body>

<div id="container">
	<div class="row">
	<div class="col-md-4"></div>
	<div class="col-md-4">
		<form class="text-center border border-light p-5" action="loginBackOffice" method="POST">

			<p class="h4 mb-4">Connexion backOffice</p>

			<p> Gestion de la prise de commande d'un restaurant </p>

			<!-- Name -->
			<input type="text" value = 'Admin1' id="defaultSubscriptionFormPassword" class="form-control mb-4" name="login">

			<!-- Email -->
			<input type="password" value='Aina' id="defaultSubscriptionFormEmail" class="form-control mb-4" name="password">

			<!-- Sign in button -->
			<button class="btn btn-success btn-block" type="submit">Login</button>
		</form>
	</div>
	</div>
</div>

</body>
</html>