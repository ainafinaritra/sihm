<?php
    $totalImpayes = 0;
?>
<table class="table table-bordered">
    <thead>
        <tr>
            <th>Date de commande</th>
            <th>Table n° </th>
            <th>Plat</th>
            <th>Prix</th>
            <th>Quantité</th>
        </tr>
    </thead>
    <tbody>
        <?php for($i=0; $i<count($commande); $i++){ ?>
        <tr>
            <td><?php echo $commande[$i]['DATECOMMANDE'] ;?></td>
            <td class="text text-right"><?php echo $commande[$i]['IDTABLES'] ;?></td>
            <td><?php echo $commande[$i]['NOMPLAT'] ;?></td>
            <td class="text text-right"><?php $totalImpayes += $commande[$i]['PRIX'];
             echo number_format($commande[$i]['PRIX'], 0) ;?></td>
            <td class="text text-right"><?php echo $commande[$i]['qte'] ;?></td>
        </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        Total impayés : <?php echo number_format($totalImpayes) ;?> Ariary
    </tfoot>
</table>