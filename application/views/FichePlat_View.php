
<div class="col-lg-4 themenu_column">
    <div class="themenu_col">
        <div class="themenu_col_title"><?php echo $fiche[0]->NOMPLAT; ?></div>
        <div class="dish_list">
           
            <form action = "AddCommande" method="post">
            <input type = "hidden" name = "idPlat" value = "<?php echo $fiche[0]->IDPLAT; ?>">
            <input type = "hidden" name = "price" value = "<?php echo $fiche[0]->PRIX; ?>">
            <div class = "form-inline">
                <h3>Prix :  </h3>
                <h3><?php echo number_format($fiche[0]->PRIX, 0); ?> Ariary </h3>
            </div>
            <div class = "form-inline">
                <h3>Quantité: </h2>
                <i class="fa fa-minus-circle" aria-hidden="true" onclick="decreaseQty()"></i><input type="text" name="quantity" id="quantityId" value="1"><i class="fa fa-plus-circle" onclick="addQuantity()" aria-hidden="true"></i>
            </div>
            <div class = "form-inline">
                <h3>Tables: </h3>
                <select name = "tableId">
                    <?php for($i = 0; $i<count($tables); $i++){ ?>
                        <option value =<?php echo $tables[$i]['IDTABLES']; ?> >Table n°<?php echo $tables[$i]['IDTABLES']." - ".$tables[$i]['NOMBREPERSONNES']; ?> personnes</option>
                    <?php } ?>
                </select>
            </div>
        </div>
            <button class="btn btn-success">Valider commande</button>
            </form>
            <button class="btn btn-default" onclick = "document.location.reload()">Annuler commande</button>
        </div>

    </div>
</div>
<script type="text/javascript">
    function addQuantity(){
        document.getElementById("quantityId").value = eval(document.getElementById("quantityId").value) + 1;
    }
    function decreaseQty(){
        document.getElementById("quantityId").value -= 1;
    }
</script>
