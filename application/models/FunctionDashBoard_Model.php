<?php

class FunctionDashBoard_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->model('Commandes_Model');
	}
	
	public function getFrequenceMensuel(){
		$query = $this->db->query("select distinct(annee), mois, sum(QTE)as qte from(select YEAR(DATECOMMANDE) as annee, MONTH(DATECOMMANDE) as mois, QTE from COMMANDES as t1)as p group by annee,mois");
		$resultat = $query->result_array();
		return $resultat;
	}
	
	public function getFrequenceJournalier(){
		$query = $this->db->query("select DATECOMMANDE , sum(QTE) as qte from COMMANDES group by DATECOMMANDE order by DATECOMMANDE asc");
		$resultat = $query->result_array();
		return $resultat;
	}
	
	public function getTop5PlatsCommander(){
		$query = $this->db->query("select PLATS.NOMPLAT, sum(COMMANDES.QTE) as QTE from COMMANDES join PLATS on COMMANDES.IDPLAT = PLATS.IDPLAT group by PLATS.IDPLAT order by QTE desc limit 5");
		$resultat = $query->result_array();
		return $resultat;
	}
	
	public function getTop5PlatsNonCommander(){
		$query = $this->db->query("select PLATS.NOMPLAT, sum(COMMANDES.QTE) as QTE from COMMANDES join PLATS on COMMANDES.IDPLAT = PLATS.IDPLAT group by PLATS.IDPLAT order by QTE asc limit 5");
		$resultat = $query->result_array();
		return $resultat;
	}
}
?>