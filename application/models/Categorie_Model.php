<?php

class Categorie_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function insert($categorie) { 
        if ($this->db->insert("CATEGORIE", $categorie)) { 
           return true; 
        } 
    } 
   
    public function delete($idcategorie) { 
        if ($this->db->delete("CATEGORIE", "IDCATEGORIE = ".$idcategorie)) { 
           return true; 
        } 
    } 
   
    public function update($categorie, $idcategorie) { 
        $this->db->set($categorie); 
        $this->db->where("IDCATEGORIE", $idcategorie); 
        $this->db->update("CATEGORIE", $categorie); 
    }
	
	public function select($idcategorie){
		$resultat = $this->db->select(*)->from('CATEGORIE')->where("IDCATEGORIE", $idcategorie)->get()->result();
		return $resultat;
	}
	
	public function select2($where){
		$query = $this->db->query("select * from CATEGORIE ".$where);
        $resultat = $query->result_array();
        return $resultat;
	}
}
?>