<?php

class Tableresto_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function insert($tableresto) { 
        if ($this->db->insert("TABLERESTO", $tableresto)) { 
           return true; 
        } 
    } 
   
    public function delete($idtables) { 
        if ($this->db->delete("TABLERESTO", "IDTABLES = ".$idtables)) { 
           return true; 
        } 
    } 
   
    public function update($tableresto, $idtables) { 
        $this->db->set($tableresto); 
        $this->db->where("IDTABLES", $idtables); 
        $this->db->update("TABLERESTO", $tableresto); 
    }
	
	public function select($idtables){
		$resultat = $this->db->select('*')->from('TABLERESTO')->where("IDTABLES", $idtables)->get()->result();
		return $resultat;
	}
	
	public function select2($where){
		$query = $this->db->query("select * from TABLERESTO ".$where);
        $resultat = $query->result_array();
        return $resultat;
	}
}
?>