<?php

class Commandes_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function insert($commandes) { 
        if ($this->db->insert("COMMANDES", $commandes)) { 
           return true; 
        } 
    } 
   
    public function delete($idcommande) { 
        if ($this->db->delete("COMMANDES", "IDCOMMANDE = ".$idcommande)) { 
           return true; 
        } 
    } 
   
    public function update($commandes, $idcommande) { 
        $this->db->set($commandes); 
        $this->db->where("IDCOMMANDE", $idcommande); 
        $this->db->update("COMMANDES", $commandes); 
    }
	
	public function select($idtables){
		$resultat = $this->db->select('*')->from('COMMANDES')->where("IDCOMMANDE", $idcommande)->get()->result();
		return $resultat;
	}
	
	public function select2($where){
		$query = $this->db->query("select * from COMMANDES ".$where);
        $resultat = $query->result_array();
        return $resultat;
	}
	
	public function getDateNow(){
		$query = $this->db->query("select date(now()) as now");
        $resultat = $query->result_array();
        return $resultat;
	}
}
?>