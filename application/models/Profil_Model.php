<?php

class Profil_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function insert($profil) { 
        if ($this->db->insert("PROFIL", $profil)) { 
           return true; 
        } 
    } 
   
    public function delete($idprofil) { 
        if ($this->db->delete("PROFIL", "IDPROFIL = ".$idprofil)) { 
           return true; 
        } 
    } 
   
    public function update($profil, $idprofil) { 
        $this->db->set($profil); 
        $this->db->where("IDPROFIL", $idprofil); 
        $this->db->update("PROFIL", $profil); 
    }
	
	public function select($idprofil){
		$resultat = $this->db->select(*)->from('PROFIL')->where("IDPROFIL", $idprofil)->get()->result();
		return $resultat;
	}
	
	public function select2($where){
		$query = $this->db->query("select * from PROFIL ".$where);
		$resultat = $query->result_array();
	}
}
?>