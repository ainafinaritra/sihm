<?php

class Facture_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function insert($facture) { 
        if ($this->db->insert("FACTURE", $facture)) { 
           return true; 
        } 
    } 
   
    public function delete($idfacture) { 
        if ($this->db->delete("FACTURE", "IDFACTURE = ".$idfacture)) { 
           return true; 
        } 
    } 
   
    public function update($facture, $idfacture) { 
        $this->db->set($facture); 
        $this->db->where("IDFACTURE", $idfacture); 
        $this->db->update("FACTURE", $facture); 
    }
	
	public function select($idfacture){
		$resultat = $this->db->select(*)->from('FACTURE')->where("IDFACTURE", $idfacture)->get()->result();
		return $resultat;
	}
	
	public function select2($where){
		$query = $this->db->query("select * from FACTURE ".$where);
		$resultat = $query->result_array();
	}
}
?>